import '../css/main.scss';

// 代码着色插件
import McTinting from '@fekit/mc-tinting';

// 视图控制器
import McView from '../../lib/mc-view';
// 新建一个代码着色实例
new McTinting();

let bbb = {
  setting: 0
};

// 实例代码
window.onload = () => {
  new McView({
    el: document.getElementById('setting'),
    watch: [
      bbb,
      'setting'
    ],
    on: {
      show() {
        console.log('回调：花儿开了');
      },
      hide() {
        console.log('回调：花儿又谢了');
      }
    },
    ev: {
      show: document.getElementById('show_setting'),
      hide: document.getElementById('hide_setting')
    }
  });
};
