import watch from '@fekit/mc-watch';

class McView {
  constructor(param = {}) {
    // 是否处理空闲状态
    this.free = 1;

    // 被接收状态的元素
    this.el = param.el;

    // 是否传入初始状态
    this.state = param.state;
    if (this.state !== undefined) {
      this.attr();
    }

    // 回调事件
    this.on = Object.assign({
      show: () => {},
      hide: () => {},
      open: () => {},
      none: () => {}
    }, param.on || {});
    console.log(this.on);

    // 是否自动监听数据
    this.watch = param.watch;
    if (this.watch && typeof this.watch[0] === 'object' && this.watch[0].hasOwnProperty(this.watch[1])) {
      this.state = this.watch[0][this.watch[1]];
      this.attr();
      watch({
        data: this.watch[0],
        item: this.watch[1],
        updated: (state) => {
          // console.log(state);
          this.data(state);
        }
      });
    }

    // 是否传入事件对象
    this.ev = param.ev;
    if (this.ev) {
      if (this.ev.show) {
        this.ev.show.addEventListener('click', () => {
          this.show();
        });
      }
      if (this.ev.hide) {
        this.ev.hide.addEventListener('click', () => {
          this.hide();
        });
      }
      if (this.ev.open) {
        this.ev.open.addEventListener('click', () => {
          this.open();
        });
      }
      if (this.ev.none) {
        this.ev.none.addEventListener('click', () => {
          this.none();
        });
      }
    }
  }

  attr() {
    // 设置显示隐藏标识状态 [跟CSS配合]
    if (this.el) {
      this.el.setAttribute('view', this.state);
    }
  }

  anim() {
    this.free = 0;
    let amEnd = (() => {
      let el = document.documentElement.style;
      let evs = { 'animation': 'animationend', 'webkitAnimation': 'webkitAnimationEnd' };
      for (let t in evs) {
        if (el[t] !== undefined) {
          return evs[t];
        }
      }
    })();

    // 显示隐藏动画结束后执行事件
    let _end = () => {
      // 动画结束后解绑动画事件防止事件累积
      this.el.removeEventListener(amEnd, _end, false);
      if (this.state === 1) {
        this.none();
        this.on.hide();
      }
      if (this.state === 2) {
        this.open();
        this.on.show();
      }
      this.free = 1;
    };
    // 显示隐藏动画
    this.el.addEventListener(amEnd, _end, false);
    this.attr();
  }

  open() {
    // console.log('open');
    if (this.free && this.state !== 3) {
      this.state = 3;
      this.attr();
      this.on.open();
    }
  }

  show() {
    // console.log('show');
    if (this.free && this.state !== 3 && this.state !== 2) {
      this.state = 2;
      this.attr();
      this.anim();
    }
  }

  hide() {
    // console.log('hide');
    if (this.free && this.state !== 0 && this.state !== 1) {
      this.state = 1;
      this.attr();
      this.anim();
    }
  }

  none() {
    // console.log('none');
    if (this.free && this.state !== 0) {
      this.state = 0;
      this.attr();
      this.on.none();
    }
  }

  data(state) {
    if (state === 0) {
      this.none();
    }
    if (state === 1) {
      this.hide();
    }
    if (state === 2) {
      this.show();
    }
    if (state === 3) {
      this.open();
    }
  }

}

export default McView;
