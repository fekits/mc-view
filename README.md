# @fekit/mc-view
```$xslt
一款数据驱动的视图控制器原生JS版本，主要用于弹框，消息，单页路由的视图控制！
```

#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)


#### 演示
[http://fekit.asnowsoft.com/plugins/mc-view](http://fekit.asnowsoft.com/plugins/mc-view)


#### 开始

下载项目: 

```npm
npm i @fekit/mc-view
```

#### 参数
```$xslt
{
  el: Element,           一个DOM元素，受控制显示隐藏的元素
  state: Number,         显示或隐藏的初始状态，如果设了watch数据监听，则这个不用 0:隐藏 1:隐藏(有动画) 2:显示(有动画) 3:显示
  watch: [               是否监听数据变化？控制视图的数据对象与对象中的指定属性名
    Object,              控制视图的数据对象
    key                  对象中的指定属性名
  ],
  on: {                  状态变化时的回调
    show() {},           显示后执行（在动画完成后执行）
    hide() {},           隐藏后执行（在动画完成后执行）
    open() {},           显示后执行
    none() {},           隐藏后执行
  },
  ev: {                  绑定点击事件的元素
    show: Element,       点击要显示的元素（有动画的）
    hide: Element,       点击要隐藏的元素（有动画的）
    open: Element,       点击要显示的元素
    none: Element,       点击要隐藏的元素
  }
}
```

#### 示例

```javascript
import McView from '@fekit/mc-view';

// 示例1：监听一个对象，并传入点击显示隐藏的事件元素
let layer = new McView({
  el: document.getElementById('setting'),
  state: 0
});
// 显示(有动画)
layer.show();
// 隐藏(有动画)
layer.hide();
// 显示(无动画)
layer.open();
// 隐藏(无动画)
layer.none();

// 监听数据并使用动据驱动时
let state = 0; // 等同于 layer.none(); 
let state = 1; // 等同于 layer.hide(); 
let state = 2; // 等同于 layer.show(); 
let state = 3; // 等同于 layer.open(); 
layer.data(state);



// 示例2：监听一个对象，并传入点击显示隐藏的事件元素
new McView({
  el: document.getElementById('setting'),
  watch: [
    bbb,
    'setting'
  ],
  on: {
    show() {
      console.log('回调：花儿开了');
    },
    hide() {
      console.log('回调：花儿又谢了');
    }
  },
  ev: {
    show: document.getElementById('show_setting'),
    hide: document.getElementById('hide_setting')
  }
});

```

#### 版本
```$xslt
v1.0.0
1. 完成了核心功能
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
email: xiaojunbo@126.com
```
